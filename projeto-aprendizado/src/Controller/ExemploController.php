<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExemploController extends AbstractController{

    /**
     * @Route("/")
     */

    public function homepage(){

        return new Response('Olá mundo');

    }

    /**
     * @Route("/mamifero/{valor}")
     */

    public function show($valor){

        $respostas = ['Esta é a primeira resposta', 'Esta é a segunda resposta', 'Esta é a terceira resposta'];

        return $this->render('mamiferos/show.twig', [
            'animal' => ucwords( str_replace('-', ' ', $valor)),
            'respostas' => $respostas
            ]
    );

    }

    /**
     * @Route("/teste-json/")
     */

     public function testeJson(){
         $respostas = new JsonResponse();
         $respostas->setData([
            'success' => true,
            'data' => [[
                'id' => 1,
                'title' => 'Aqui teste 1'
            ],
            [
                'id' => 2,
                'title' => 'teste 2'
            ]
            ]
         ]);
         return $respostas;
     }
}